var panoramas = [];
var viewer_main;

class Pano360 {
    constructor(source, links = []) {
        this.item = new PANOLENS.ImagePanorama(source);
        links.forEach(link => {
            this.item.link(link.link, link.position);
        });
    }
    add(source) {
        this.item.add(source);
    }
    addLinks(links) {
        links.forEach(link => {
            this.item.link(link.item, link.position);
        });
    }
    addLink(link, position) {
        this.item.link(link.item, position.vector3);
    }
}
class Link {
    constructor(item, position) {
        this.item = item.item;
        this.position = position.vector3;
    }
}
class Vector3 {
    constructor(x = 0, y = 0, z = 0) {
        this.vector3 = new THREE.Vector3(x, y, z);
    }
}

run = () => {
    viewer_main = new PANOLENS.Viewer({
        enableReticle: false,
        output: 'console',
        viewIndicator: true,
        autoRotate: false,
        autoRotateSpeed: 2,
        autoRotateActivationDuration: 6000,
        dwellTime: 1000
    });
    init();
    panoramas.forEach(pan => {
        viewer_main.add(pan.item);
    });
    viewer_main.enableControl(PANOLENS.CONTROLS.DEVICEORIENTATION);
    viewer_main.enableEffect(PANOLENS.MODES.CARDBOARD);
    viewer_main.enableControl(PANOLENS.CONTROLS.ORBIT);
    viewer_main.enableEffect(PANOLENS.MODES.NORMAL);
}

// Добавление новых панорам нужно производить внутри функции init
//  Чтобы добавить панораму в коллекцию нужно написать panoramas.push(new Pano360("image.jpg", [new Link(панорама, new Vector(x,y,z)), ...]?));
//
//  Чтобы добавить ссылки можно воспользоваться однким из двух методов:
//      panoramas[i].addLink(panoramas[i], new Vector3(x, y, z));
//      panoramas[i].addLinks([new Link(panoramas[i], new Vector3(x, y, z)), new Link(panoramas[i], new Vector3(x, y, z)), ...]);
//  Первая панорама - главная
init = () => {
    panoramas.push(new Pano360("images/building.jpg"));
    panoramas.push(new Pano360("images/pier4000x2000.jpg"));

    panoramas[0].addLinks([new Link(panoramas[1], new Vector3(3500, 0, 5000))]);
    panoramas[1].addLink(panoramas[0], new Vector3(3500, 0, 5000));
}

run();